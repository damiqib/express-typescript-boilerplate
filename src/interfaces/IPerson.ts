export interface IPerson {
  id: number
  name: { first: string; last: string }
}
