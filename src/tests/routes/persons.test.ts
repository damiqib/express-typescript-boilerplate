import { describe, it, expect } from '@jest/globals'
import supertest from 'supertest'

import { app } from '../../app'

describe('/persons', () => {
  it('should return persons', async () => {
    const result = await supertest(app).get('/api/v1/persons')

    expect(result.body.success).toEqual(true)
    expect(result.body.data.length).toEqual(1)
    expect(result.status).toEqual(200)
  })
})
