import { Errback, Request, Response, NextFunction } from 'express'

import { app } from './app'
import { IApiResponse } from './interfaces/IApiResponse'
import { IError } from './interfaces/IError'

const PORT = process.env.PORT || 3131

//Error handling
app.use(
  (
    error: Errback,
    req: Request,
    res: Response<IApiResponse<IError>>,
    // eslint-disable-next-line
    next: NextFunction
  ) => {
    res.status(500).json({
      success: false,
      message: 'Unknown internal server error occurred, please try again.',
      data: {
        stack: error,
      },
    })
  }
)

//Listen
app.listen(<string>PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server listening on ${PORT}`)
})
