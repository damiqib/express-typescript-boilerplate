import express, { Application } from 'express'
import helmet from 'helmet'
import cors from 'cors'

import routes from './routes'

const app: Application = express()

//Helmet
app.use(helmet())

//Cors
app.use(cors())

//Routes
app.use('/', routes)

export { app }
