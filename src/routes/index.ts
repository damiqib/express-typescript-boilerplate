import express from 'express'

import { persons } from './persons'

const routes = express.Router()

routes.use('/api/v1/persons', persons)

export default routes
