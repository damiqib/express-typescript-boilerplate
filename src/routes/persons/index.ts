import express, { Request, Response } from 'express'

import { IApiResponse } from '../../interfaces/IApiResponse'
import { IPerson } from '../../interfaces/IPerson'

const persons = express.Router()

persons.get('/', (req: Request, res: Response<IApiResponse<IPerson[]>>) => {
  return res.json({
    success: true,
    message: 'Persons',
    data: [{ id: 1, name: { first: 'Simon', last: 'Excellent' } }],
  })
})

export { persons }
