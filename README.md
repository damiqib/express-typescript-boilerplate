# Hop on to TypeScript Express

**Express TypeScript Boilerplate** comes with easy to use tools to allow a quick start on your backend.

- NodeJS
- TypeScript
- Express
  - Helmet
  - Cors
  - Dotenv
- Jest
- Supertest
- Eslint
- Nodemon

Project has a basic setup for a /persons-route, interfaces and tests

## How to get started

```
# Fork or clone the repo

# Edit these to match your needs
- .eslintrc.json
  - rules: {}
- .prettierrc.json
- package.json
  - name, version, author, license, etc...
...

# Set port either in ./src/index.ts or create a .env file with PORT=3131
- By default the app will run on port 3131

# Install dependencies
yarn install

# Run available yarn commands
```

## Available yarn commands

```
yarn run dev
- start nodemon
- using the default setup, you can access the /persons-route at
  http://localhost:3131/api/v1/persons

yarn run test
- run tests

yarn run test:watch
- continuously run tests on file change

yarn run lint
- eslint

yarn run lint:watch
- continuously run eslint on file change

yarn run build
- make production build

yarn run start
- start the production app

```

## Husky hooks

The app has a pre-commit hook setup to run eslint and prettier fixes, tests and linting before commit.

## GitLab CI

Example .gitlab-ci.yml included. Has a pipeline for running tests/lints and build.
